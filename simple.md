- [Etiam vel tortor sodales tellus ultricies commodo.](#markdown-header-done-etiam-vel-tortor-sodales-tellus-ultricies-commodo.-testnovo)
- [Lists ](#markdown-header-lists)
- [Links](#markdown-header-links)
- [Code samples](#markdown-header-code-samples)
      - [Bash](#markdown-header-bash)
      - [Images](#markdown-header-images)
- [Tables](#markdown-header-tables)
- [Tables](#markdown-header-tables)

[TOC]


# **DONE** Etiam vel tortor sodales tellus ultricies commodo.     `:test:novo:`

Pellentesque dapibus suscipit ligula. Donec posuere augue in quam. Etiam vel tortor sodales tellus ultricies commodo. Suspendisse potenti. Aenean in sem ac leo mollis blandit. Donec neque quam, dignissim in, mollis nec, sagittis eu, wisi. Phasellus lacus. Etiam laoreet quam sed arcu. Phasellus at dui in ligula mollis ultricies. Integer placerat tristique nisl. Praesent augue. Fusce commodo. Vestibulum convallis, lorem a tempus semper, dui dui euismod elit, vitae placerat urna tortor vitae lacus. Nullam libero mauris, consequat quis, varius et, dictum id, arcu. Mauris mollis tincidunt felis. Aliquam feugiat tellus ut neque. Nulla facilisis, risus a rhoncus fermentum, tellus tellus lacinia purus, et dictum nunc justo sit amet elit.

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec hendrerit tempor tellus. Donec pretium posuere tellus. Proin quam nisl, tincidunt et, mattis eget, convallis nec, purus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla posuere. Donec vitae dolor. Nullam tristique diam non turpis. Cras placerat accumsan nulla. Nullam rutrum. Nam vestibulum accumsan nisl.


# Lists 

Features to implent :

-   [ ] code samples
-   [ ] images
-   [X] tabelas
-   [-] TOC ( with links )
-   [-] Links
    -   [ ] when header contain tags ( currently we don't suport link to headings containing tags )

-   [ ] Better format - TODO keywords
-   [ ] inline code
-   [ ] statistics cookie


# Links

A direct link: <https://duckduckgo.com/>

[Bibucket's markdown](https://bitbucket.org/tutorials/markdowndemo/src/master/)

[Wikipedia](https://en.wikipedia.org/wiki/Precision_and_recall)

Internal link to other page in same repo: [README](readme.md)

Internal link to same page: [Tables](#markdown-header-tables)

Other link: [Tables](#markdown-header-tables)


# Code samples

This is an inline code : `Teste`.

Inline evaluated code `Hello World`


## Bash

```sh
mkdir -p /tmp/test
cd /tmp/test
touch hello.txt
ls 
```

    
    sh-5.0$ sh-5.0$ hello.txt


## Images

```R
library('tidyverse')
library(ggplot2)
ggplot(midwest, aes(x=area, y=poptotal)) + geom_point()
```

![img](image.png)


# Tables

| Item  | Quantity | Price |
|----- |-------- |----- |
| Milk  | 2        | 4.0   |
| Bread | 3        | 7.5   |
| sugar | 1        | 2.5   |
| Total |          | 14.   |


# Tables

| Item  | Quantity | Price |
|----- |-------- |----- |
| Milk  | 2        | 4.0   |
| Bread | 3        | 7.5   |
| sugar | 1        | 2.5   |
| Total |          | 14.   |
